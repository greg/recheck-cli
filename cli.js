#!/usr/bin/env node

const fs = require('fs').promises;
const { glob } = require('glob');
const meow = require('meow');
const { checkSync } = require('recheck');

const cli = meow(`
  Usage
    $ recheck [arguments] "<dir glob>"

  Flags
    -n, --node-modules  include node_modules (default: false)

  Examples
    $ recheck "**/*.js"
    $ recheck -n "**/*.js"
`, {
  importMeta: import.meta,
  flags: {
    nodeModules: {
      type: 'boolean',
      shortFlag: 'n',
    },
  },
});

const [globPattern] = cli.input;
const { flags } = cli;

if (!globPattern && process.stdin.isTTY) {
  console.error('Path is required');
  process.exit(1);
}

const getFiles = async (globPattern) => {
  const ignoreNodeModules = [
    'node_modules',
    '**/node_modules/**',
    '**/node_modules',
    './node_modules',
    './node_modules/**',
    'node_modules/**',
  ];
  const files = await glob(globPattern, {
    ignore: !flags.nodeModules && ignoreNodeModules,
  });
  return files;
};

const parseRegexes = async (file) => {
  const fileContents = await fs.readFile(file, 'utf8');
  const fileLines = fileContents.split('\n');

  const re = /\/((?![*+?])(?:[^\r\n\[/\\]|\\.|\[(?:[^\r\n\]\\]|\\.)*\])+)\/((?:g(?:im?|mi?)?|i(?:gm?|mg?)?|m(?:gi?|ig?)?)?)/;

  return fileLines.reduce((obj, line, index) => {
    if (re.test(line)) {
      const foundRegex = line.match(re);
      try {
        const checkResult = checkSync(foundRegex[0].slice(1, -1), '', {
          timeout: 1000,
        });
        if (checkResult.status !== 'safe' && checkResult.status !== 'unknown') {
          if (!obj[file]) {
            obj[file] = [];
          }
          obj[file].push({
            lineNr: index + 1,
            line: line.trim(),
            summary: checkResult.complexity?.summary ?? '',
          });
        }
      } catch (error) {
        console.warn(`Warning: Error checking regex in ${file} at line ${index + 1}: ${error.message}`);
      }
    }
    return obj;
  }, {});
};

const main = async () => {
  console.log(`
recheck results:

> Go to https://makenowjust-labo.github.io/recheck/ for more details
> or if you want to double-check the matched regexes.

--- 
`);

  const files = await getFiles(globPattern);
  console.log(`Checking ${files.length} files...`);
  console.log();

  const results = await Promise.all(files.map(parseRegexes));

  results
    .filter((item) => Object.keys(item).length !== 0)
    .forEach((entry) => {
      const [file, issues] = Object.entries(entry)[0];
      console.log(`File: ${file}`);
      console.log(`  Unsafe Regex`);
      issues.forEach((line) => {
        console.log(`    - (L${line.lineNr}) ${line.line}`);
        console.log(`      - Summary: ${line.summary}`);
      });
      console.log();
    });
};

main().catch((error) => {
  console.error('An error occurred:', error);
  process.exit(1);
});